FridgeBot
=====

These are the design files and firmware for the FridgeBot temperature controller developed by Kyle Klaus.

These files have been made available online through a [Creative Commons Attribution-ShareAlike 3.0](http://creativecommons.org/licenses/by-sa/3.0/) license.

You are welcome to distribute, remix, and use these files for commercial purposes. If you do so, please attribute the original design to Kyle Klaus both on the website and on the physical packaging of the product or in the instruction manual. All derivative works must be published under the same or a similar license.

The FridgeBot brand, logo, design of the website and design of the boards are copyright of Kyle Klaus and cannot be used without formal permission. For informations about the right way to use them, please write to kklaus@indemnity83.com

## WARNING: High voltage

If you choose to use these design files, please be forewarned: this system involves working with mains (high voltage) designs, and can be extremely dangerous. Please proceed with extreme caution. We cannot guarantee the safety of these designs, as they had not yet been put through certification. If you download these files, you are taking upon yourself any liability for injury or property damage.

## Features
- **An integrated approach**. Designed to replace the in-percise (and possibly innacurate) thermostat in your fridge with a precise digital system for controling temperature
- **Simple Interface**. Two big 4-digit, 7-segment displays show the set-point and current temperature so you can always see whats going on and a simple rotary encoder knob for adjusting the setpoint

## Assumptions and Requirements
- **Self Protecting Compressor**. The control mechanism assumes the compressor of your fridge will cycle itself for protection (minimum run times, minimum cooldown, maximum compressor temperature, etc). The FrideBot will simply tell the compressor when it needs cooling and when it doesn't. 
- **Relay Contactor**. The mechanism used to tell the compressor to run is a relay contact. Typically the 120V mains is run through the thermostat

## Basic structure

The design of the FridgeBot includes six major subsystems, each generally discrite for the initial design. Later designs should integrate these components into an overall hardware design. 

- **Power supply**. For simplicity, a "wall wort" style power supply is used to provide the electronics with 5V DC.
- **Micro-controller**. FridgeBot is powered by an Arduino. Any standard arduino should work, but the system is tested against the Arduino Uno and similar devices. 
- **Relay Contact**. You can either replace the thermostat in your fridge with a properly sized relay, or turn the thermostat to its coldest setting and use a Powerswitch Tail 2 (http://www.adafruit.com/products/268) 
- **Displays**. http://www.adafruit.com/products/878 or similar should work. You'll need to set the address on one by closing a solder jumper, the other can remain on the default address. 
- **Temperature Sensor**. The sensor used is a simple thermistor, ADC and communication to the arduino is handled over I2C using a 12-bit chip http://www.adafruit.com/products/1083 
- **Rotary Encoder**. Just about any rotary encoder will do, but this one from Adafruit is the one I used and it works great http://www.adafruit.com/products/377
