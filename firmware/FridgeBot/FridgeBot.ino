/**************************************************************************/
/*!
    @file     FridgeBot.ino
    @author   Kyle Klaus
    @license  Creative Commons Attribution-ShareAlike 3.0
    
    @section  HISTORY

    v1.0 - First release
*/
/**************************************************************************/

/**************************************************************************/
/*! 
    @brief  Enable debug (serial) output
*/
/**************************************************************************/
#define DEBUG 

/**************************************************************************/
/*! 
    @brief  Define the pins and addresses for sensors and devices
*/
/**************************************************************************/
#define PIN_ENCODER_A 3
#define PIN_ENCODER_B 2
#define PIN_RELAY 7
#define HUMD_DISP_ADDR 0x70
#define TEMP_DISP_ADDR 0x71
#define PIN_DHT 4
#define DHT_TYPE DHT22

/**************************************************************************/
/*! 
    @brief  Misc. applicaiton variables. Adjust these to suit your
            particular sensors and preferences
*/
/**************************************************************************/
#define FRIDGE_RUN_WINDOW 2         // Temperature window in degrees F to target from setpoint
#define TICKRATE 1000              // Defines the approx frequency of application ticks
#define EEPROMADDR 0                // The starting memory address for saving setPoint
#define MAXTEMP 75                  // The upper end (inclusive) of the setpoint
#define MINTEMP 32                  // The lower end (inclusive) of the setpoint
#define DEFAULTBRIGHTNESS 3         // The default brightness of the screens
#define ALTBRIGHTNESS 15            // The alternate brightness of the screens

/**************************************************************************/
/*! 
     ------------------  DO NOT EDIT BELOW THIS LINE   ------------------  
*/
/**************************************************************************/

/*=========================================================================
    Application Helpers
    -----------------------------------------------------------------------*/
    #define SECOND 1000
    #define MINUTE 60000
    #define MAXTIMINGS 85                    // DHT Timing transitions
    #define DHT11 11
    #define DHT22 22
    #define DHT21 21
    #define AM2301 21
    #define BAD_HUM    -1                    // Bad humitidy reading
    #define BAD_TEMP -999                    // Bad temperature reading    
    
/*=========================================================================
    Android Standard Libraries
    -----------------------------------------------------------------------*/
    #include "avr/eeprom.h"                 // AVR EEPROM memory Library
    #include "Wire.h"                       // Arduino I2C Library    
    
/*=========================================================================
    Application Libraries
    -----------------------------------------------------------------------*/
    #include "Encoder.h"                    // Rotary Encoder Library
    #include "Timer.h"                      // Timer & Event Library
    #include "Display.h"                    // HT16K33 Driven 7-Seg Display Library

/*=========================================================================
    Application Variables
    -----------------------------------------------------------------------*/
    volatile int     setPoint;              // Application setPoint
    volatile int     eepromSaveEvent;       // The delay event for saving to eeprom
    volatile int     tempReading;           // Application Temperature    
    volatile int     humdReading;           // Application Humidity    
    volatile boolean fridgeCanStart;        // Defines if the fridge can start
    volatile boolean fridgeCanStop;         // Defines if the fridge can stop
    volatile boolean fridgeRunning;         // Defines if the fridge is running or stopped  
    volatile boolean canSample;             // Defines if temp/humd sampleing should occur
    volatile boolean firstReading;          // Used to determine if the DHT reading has occured
    volatile long    lastReadTime;          // How long since the last reading             
    volatile int     data[6];               // DHT sensor data array
    volatile int     _count;                // DHT 

/*=========================================================================
    Application Objects
    -----------------------------------------------------------------------*/
    Encoder     setpEncoder  = Encoder(PIN_ENCODER_A, PIN_ENCODER_B);
    Display     humdDisplay  = Display();
    Display     tempDisplay  = Display();
    Timer       memoryTimer  = Timer();
    Timer       fridgeTimer  = Timer();
    Timer       appTimer     = Timer();

/**************************************************************************/
/*! 
    @brief  Setup the application
*/
/**************************************************************************/
    void setup() {
      
      // Say Hello, fridgeBot!
      #ifdef DEBUG
        Serial.begin(9600);
        Serial.println(F("\nHello, fridgeBot\n")); 
      #endif  
      
      // Initialize Pins
      pinMode(PIN_RELAY, OUTPUT);
      pinMode(PIN_DHT, INPUT);
      digitalWrite(PIN_DHT, HIGH);
      
      // Initialize Variables
      canSample = true;
      fridgeRunning = false;
      lastReadTime = 0;
      
      // Setup the application timer  
      appTimer.every(TICKRATE, tick);      
      
      // Start I2C communications with each device
      tempDisplay.begin(TEMP_DISP_ADDR);    
      humdDisplay.begin(HUMD_DISP_ADDR);
      
      // Adjust screen brightness
      tempDisplay.setBrightness(DEFAULTBRIGHTNESS);
      humdDisplay.setBrightness(DEFAULTBRIGHTNESS);   
      
      // Initial setpoint, read in from memory
      readSetPoint();
      
      // Get an inital temperature reading
      sampleTemperature();

      // Setup complete
      #ifdef DEBUG
        Serial.println("\nSetup complete\n");
      #endif
    }

/**************************************************************************/
/*! 
    @brief  Main application loop
*/
/**************************************************************************/
void loop() {
  
  // Check timed actions
  memoryTimer.update();
  fridgeTimer.update();
  appTimer.update();
  
  // Handle new value from Rotary Encoder
  int newSetPoint = setpEncoder.read();
  if (newSetPoint != setPoint) {     
    if (newSetPoint < MINTEMP) {
      #ifdef DEBUG
        Serial.println("Out of Range");
      #endif  
      newSetPoint = MINTEMP;
    } else if (newSetPoint > MAXTEMP) {
      #ifdef DEBUG
        Serial.println("Out of Range");
      #endif  
      newSetPoint = MAXTEMP;  
    }
    
    // Update the new setPoint
    updateSetPoint(newSetPoint);
  }
  
}

/**************************************************************************/
/*! 
    @brief  Application tick (sorta like the loop, but non-blocking and 
            timer triggered)
*/
/**************************************************************************/
void tick() {
 sampleTemperature();
 
   // Get the upper and lower limits based on setPoint and run window
  int upperLimit = (setPoint + FRIDGE_RUN_WINDOW);
  int lowerLimit = (setPoint - FRIDGE_RUN_WINDOW);
  
  #ifdef DEBUG 
    Serial.print(F("Operation Window: "));
    Serial.print(lowerLimit);
    Serial.print(F(" *F to "));
    Serial.print(upperLimit);
    Serial.println(F(" *F"));
  #endif
  
  // Run the fridge! (if we need to)
  if( upperLimit < tempReading) {
    #ifdef DEBUG    
    Serial.println("Start fridge called");
    #endif     
    startFridge();
  } else if( lowerLimit > tempReading) {
    #ifdef DEBUG    
    Serial.println("Stop fridge called");
    #endif         
    stopFridge();
  }     
}

/**************************************************************************/
/*! 
    @brief  Reads the setPoint variable from EEPROM memory
*/
/**************************************************************************/
unsigned int readSetPoint() {
  unsigned int word = word(eeprom_read_byte((unsigned char *)EEPROMADDR), 
                           eeprom_read_byte((unsigned char *)EEPROMADDR+1));
  updateSetPoint(word, false);
}

/**************************************************************************/
/*! 
    @brief  Writes the setPoint variable to EEPROM memory
*/
/**************************************************************************/
void writeSetPoint() {
  eeprom_write_byte((unsigned char *)EEPROMADDR,highByte(setPoint));
  eeprom_write_byte((unsigned char *)EEPROMADDR+1,lowByte(setPoint)); 
  
  // Blink the display to let the user know we saved it
  tempDisplay.clear();
  tempDisplay.writeDisplay();
  delay(300);
  tempDisplay.write(setPoint);
  tempDisplay.writeDisplay();
  delay(300);
  
  // Return to our normally scheduled programming
  tempDisplay.write(tempReading);
  tempDisplay.writeDisplay();
  canSample = true; 
  
  #ifdef DEBUG
    Serial.print(F("SetPoint "));
    Serial.print(setPoint);
    Serial.println(" *F Saved to EEPROM");
    Serial.print(F("Operation Window: "));
    Serial.print(setPoint - FRIDGE_RUN_WINDOW);
    Serial.print(F(" *F to "));
    Serial.print(setPoint + FRIDGE_RUN_WINDOW);
    Serial.println(F(" *F"));
  #endif     
}

/**************************************************************************/
/*! 
    @brief  Takes a temperature sample reading
*/
/**************************************************************************/
void sampleTemperature() {
  if( !canSample ) return; 
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  int8_t h = readHumidity();
  int16_t t = readTemperature(1);
  
  // check if returns are valid before assigning them to app values
  if ( t == BAD_TEMP || h == BAD_HUM ) {
    Serial.println(F("Failed to read from DHT"));
  } else {
    tempReading = t * 1.8 + 32;
    humdReading = h;
  }

  // Update application and UI
  tempDisplay.write(tempReading);
  tempDisplay.writeDisplay();
  humdDisplay.write(humdReading);
  humdDisplay.writeDisplay();
  
  // Debug output of temperature sample
  #ifdef DEBUG
    Serial.print(F("Humidity: ")); 
    Serial.print(humdReading);
    Serial.print(F(" %\t"));
    Serial.print(F("Temperature: ")); 
    Serial.print(tempReading);
    Serial.println(F(" *F")); 
  #endif
}

/**************************************************************************/
/*! 
    @brief  Updates the application setpoint, sets all appropriate variables
            and UI elements. 
            
            Overloaded
*/
/**************************************************************************/
void updateSetPoint(int value) { updateSetPoint(value, true); }
void updateSetPoint(int value, boolean andSave) {
    // Store the setPoint
    setPoint = value;

    // Update system objects with new setPoint
    setpEncoder.write(setPoint); 
    tempDisplay.write(setPoint);
    tempDisplay.writeDisplay();
    
    if (andSave) {
      // Increase the brightness on the display to indicate 
      // to the user that the setpoint hasn't been saved
      tempDisplay.setBrightness(ALTBRIGHTNESS);
      
      // Disable samping so it doesn't interfere
      canSample = false;      
      
      // setup or reset a timer to save the value to memory
      memoryTimer.stop(eepromSaveEvent);
      eepromSaveEvent = memoryTimer.after(5 * SECOND, writeSetPoint);  
    }
    
    // Debug output of new setPoint
    #ifdef DEBUG
      Serial.print(F("SetPoint "));
      Serial.print(setPoint);
      Serial.println(" *F");
    #endif    
}

/**************************************************************************/
/*! 
    @brief  Stop the fridge
    
            Requires that the fridge can stop, when the fridge stops it 
            needs to start a cool down timer based on FRIDGE_MIN_OFF and 
            cancel any running fridgeOverrunEvent based on FRIDGE_MAX_ON
*/
/**************************************************************************/
void stopFridge() {
  if (fridgeRunning) {
    digitalWrite(PIN_RELAY, LOW);
    fridgeRunning = false;
    tempDisplay.setBrightness(DEFAULTBRIGHTNESS);    
    #ifdef DEBUG
      Serial.println(F("Fridge Stopped"));
    #endif 
  } 
}

/**************************************************************************/
/*! 
    @brief  Start the fridge
    
            Requires that the fridge can start, when the fridge starts it 
            needs to run for at least FRIDGE_MIN_ON and start a long run 
            timer to shut the fridge down if it exceeds FRIDGE_MAX_ON
*/
/**************************************************************************/
void startFridge() { 
  if (fridgeCanStart && !fridgeRunning) {
    digitalWrite(PIN_RELAY, HIGH);
    fridgeRunning = true;     
    tempDisplay.setBrightness(ALTBRIGHTNESS);    
    #ifdef DEBUG
      Serial.println(F("Fridge Started"));
    #endif 
  } 
}

/**************************************************************************/
/*! 
    @brief  Read from the DHT sensor
*/
/**************************************************************************/
boolean read(void) {
  uint8_t laststate = HIGH;
  uint8_t counter = 0;
  uint8_t j = 0, i;
  unsigned long currenttime;

  // pull the pin high and wait 250 milliseconds
  digitalWrite(PIN_DHT, HIGH);
  delay(250);

  currenttime = millis();
  if (currenttime < lastReadTime) {
    // ie there was a rollover
    lastReadTime = 0;
  }
  if (!firstReading && ((currenttime - lastReadTime) < 2000)) {
    return true; // return last correct measurement
    //delay(2000 - (currenttime - _lastreadtime));
  }
  firstReading = false;
  /*
    Serial.print("Currtime: "); Serial.print(currenttime);
    Serial.print(" Lasttime: "); Serial.print(_lastreadtime);
  */
  lastReadTime = millis();

  data[0] = data[1] = data[2] = data[3] = data[4] = 0;
  
  // now pull it low for ~20 milliseconds
  pinMode(PIN_DHT, OUTPUT);
  digitalWrite(PIN_DHT, LOW);
  delay(20);
  cli();
  digitalWrite(PIN_DHT, HIGH);
  delayMicroseconds(40);
  pinMode(PIN_DHT, INPUT);

  // read in timings
  for ( i=0; i< MAXTIMINGS; i++) {
    counter = 0;
    while (digitalRead(PIN_DHT) == laststate) {
      counter++;
      delayMicroseconds(1);
      if (counter == 255) {
        break;
      }
    }
    laststate = digitalRead(PIN_DHT);

    if (counter == 255) break;

    // ignore first 3 transitions
    if ((i >= 4) && (i%2 == 0)) {
      // shove each bit into the storage bytes
      data[j/8] <<= 1;
      if (counter > _count)
        data[j/8] |= 1;
      j++;
    }

  }

  sei();
  
  /*
  Serial.println(j, DEC);
  Serial.print(data[0], HEX); Serial.print(", ");
  Serial.print(data[1], HEX); Serial.print(", ");
  Serial.print(data[2], HEX); Serial.print(", ");
  Serial.print(data[3], HEX); Serial.print(", ");
  Serial.print(data[4], HEX); Serial.print(" =? ");
  Serial.println(data[0] + data[1] + data[2] + data[3], HEX);
  */

  // check we read 40 bits and that the checksum matches
  if ((j >= 40) && 
      (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {
    return true;
  }
  

  return false;

}

/**************************************************************************/
/*! 
    @brief  Read Temperature from DHT Sensor
    
            boolean S == Scale.  True == Farenheit; False == Celcius
*/
/**************************************************************************/
int16_t readTemperature(bool S) {
  int16_t f;

  if (read()) {
    switch (DHT_TYPE) {
    case DHT11:
      f = (int16_t) data[2];
      if(S)
      	f = convertCtoF(f); 
      	
      return f;
    case DHT22:
    case DHT21:
      f = (int16_t)(data[2] & 0x7F);
      f *= 256;
      f += (int16_t) data[3];
      f /= 10;
      if (data[2] & 0x80)
	   f *= -1;
      if(S)
	   f = convertCtoF(f);

      return f;
    }
  }
  /* Serial.print("Read fail"); */
  return BAD_TEMP; // Bad read, return value (from TinyDHT.h)
}

/**************************************************************************/
/*! 
    @brief  Convert Celsius to Fahrenheit
*/
/**************************************************************************/
int convertCtoF(int c) {
  return (c * 9) / 5 + 32;
}

/**************************************************************************/
/*! 
    @brief  Read Humidity from DHT Sensor
   
*/
/**************************************************************************/
uint8_t readHumidity(void) {  //  0-100 %
  uint8_t f;
  uint16_t f2;  // bigger to allow for math operations
  if (read()) {
    switch (DHT_TYPE) {
    case DHT11:
      f = data[0];
      return f;
    case DHT22:
    case DHT21:
      f2 = (uint16_t) data[0];
      f2 *= 256;
      f2 += data[1];
      f2 /= 10;
      f = (uint8_t) f2;
      return f;
    }
  }
  /* Serial.print("Read fail"); */
  return BAD_HUM; // return bad value (defined in TinyDHT.h)
}
